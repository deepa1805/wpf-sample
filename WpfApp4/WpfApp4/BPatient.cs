﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace WpfApp4
{
    class BPatient
    {
        Patient p;
        private ICommand _addPatientCmd;
        private ICommand _addPatientCmd1;
        private ViewModel _viewModel;

        private string strType;

        public BPatient()
        {
            p = new Patient();
            _addPatientCmd = new RelayCommand<string>(Add1, CanAdd);
            _addPatientCmd1 = new RelayCommand<string>(Add2, CanAdd);

        }
        public bool CanAdd(object obj)
        {
            //Enable the Button only if the mandatory fields are filled

            return true;
        }

        /// <summary>
        /// Gets or Sets Unique integer ID for the Patient
        /// </summary>
        public int Id { get { return p.Id; } set { p.Id = value; } }

        /// <summary>
        /// Gets or Sets Name of the Patient
        /// </summary>
        public string Name { get { return p.Name; } set { p.Name = value; } }

        public string StrType { get { return strType; } set { strType = value; } }

        public ViewModel CViewMode { get { return _viewModel; } set { _viewModel = value; } }

        

        /// <summary>
        /// Gets or Sets MobileNumber of the Patient
        /// </summary>
        public Int64 MobileNumber { get { return p.MobileNumber; } set { p.MobileNumber = value; } }

        public bool IsActive { get { return p.IsActive; } set { p.IsActive = value;   } }

        public void Add1(object obj)
        {
            string str = obj as string;
            if (StrType == null || StrType == string.Empty )
                StrType = str;
            if (StrType == "chk")
            {
               // MessageBox.Show("Success");
                StrType = "cmb";
                //CViewMode.BPatients.Count
                CViewMode.CmbSelIndex = 2;
            }
            
        }

        public void Add2(object obj)
        {
            string str = obj as string;
            if (StrType == null || StrType == string.Empty)
                StrType = str;
            if (StrType == "cmb")
            {
               // MessageBox.Show("Hey");
                StrType = "chk";
            }

        }

        public ICommand AddPatientCmd { get { return _addPatientCmd; } }

        public ICommand AddPatientCmd1 { get { return _addPatientCmd1; } }
    }
}
