﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace WpfApp4
{
    class ViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<BPatient> _bpatients;
        private  Patient domObject;
       
        private  ObservableCollection<BPatient> _patients;
        private  ICommand _addPatientCmd;


        public ViewModel()
        {
            domObject = new Patient();
           
          
            _patients = new ObservableCollection<BPatient>();
            _bpatients = new ObservableCollection<BPatient>();
            Add();
          

        }

      

      

        /// <summary>
        /// Add operation of the AddPatientCmd.
        /// Operation that will be performormed on the control click.
        /// </summary>
        /// <param name="obj"></param>
        public void Add()
        {
            //Always create a new instance of patient before adding. 
            //Otherwise we will endup sending the same instance that is binded, to the BL which will cause complications
            var patient = new BPatient { Id = 10, Name = "abc", MobileNumber = 123, IsActive =false, CViewMode = this };
          
           
                Patients.Add(patient);
           
            patient = new BPatient { Id = 20, Name = "xyz", MobileNumber = 123, IsActive = true, CViewMode = this };
            Patients.Add(patient);

             patient = new BPatient { Id = 10, Name = "abc", MobileNumber = 123, IsActive = true, CViewMode = this };
            
                Patients.Add(patient);
           
            patient = new BPatient { Id = 20, Name = "xyz", MobileNumber = 123, IsActive = true, CViewMode = this };
            BPatients.Add(patient);
        }

        int _cmbSelIndex;
        public int CmbSelIndex
        {
            get { return _cmbSelIndex; }
            set { _cmbSelIndex = value; OnPropertyChanged("CmbSelIndex"); }

        }


        /// <summary>
        /// Gets the Patients. Used to maintain the Patient List.
        /// Since this observable collection it makes sure all changes will automatically reflect in UI 
        /// as it implements both CollectionChanged, PropertyChanged;
        /// </summary>
        public ObservableCollection<BPatient> Patients { get { return _patients; } set { _patients = value; } }

        public ObservableCollection<BPatient> BPatients { get { return _bpatients; } set { _bpatients = value; } }


        #region INotifyPropertyChanged Members

        /// <summary>
        /// Event to which the view's controls will subscribe.
        /// This will enable them to refresh themselves when the binded property changes provided you fire this event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// When property is changed call this method to fire the PropertyChanged Event
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(string propertyName)
        {
            //Fire the PropertyChanged event in case somebody subscribed to it
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
